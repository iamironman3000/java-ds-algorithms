package com.ds.algos.arrays;

import java.util.Arrays;

public class ReverseArray {
    public void reverse(int[] arr) {
        int low = 0, high = arr.length - 1;
        while (low < high) {
            int temp = arr[low];
            arr[low] = arr[high];
            arr[high] = temp;
            low++;
            high--;
        }

        System.out.println(Arrays.toString(arr));
    }

    public static void main(String[] args) {
        ReverseArray sol = new ReverseArray();
        sol.reverse(new int[] {5, 4, 3, 2, 1});
    }
}

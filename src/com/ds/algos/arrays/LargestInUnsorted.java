package com.ds.algos.arrays;

public class LargestInUnsorted {

    public int search(int arr[]) {
        int result = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > result) {
                result = arr[i];
            }
        }

        return result;
    }
}

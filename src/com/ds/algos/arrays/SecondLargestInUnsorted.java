package com.ds.algos.arrays;

public class SecondLargestInUnsorted {

    // 1, 4, 2, 5, 8 , 19, 12
    public int search(int arr[]) {
        int result = -1, largest = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > largest) {
                result = largest;
                largest = arr[i];
            } else if (arr[i] != largest) {
                if (result == -1 || arr[i] > result) {
                    result = arr[i];
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        SecondLargestInUnsorted sol = new SecondLargestInUnsorted();

        System.out.println(sol.search(new int[] {1, 4, 2, 5, 8 , 19, 12}));
    }
}

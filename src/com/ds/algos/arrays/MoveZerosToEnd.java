package com.ds.algos.arrays;

import java.util.Arrays;

public class MoveZerosToEnd {
    public void moveZeroes(int[] arr) {
        int refIndex = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != 0) {
                int temp = arr[refIndex];
                arr[refIndex] = arr[i];
                arr[i] = temp;
                refIndex++;
            }
        }
    }

    public static void main(String[] args) {
        MoveZerosToEnd sol = new MoveZerosToEnd();
        int[] arr = new int[] {0, 1, 2, 3, 4, 0, 6, 0, 9};
        sol.moveZeroes(arr);
        System.out.println(Arrays.toString(arr));

    }
}

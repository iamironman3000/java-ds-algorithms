package com.ds.algos.arrays;

import java.util.Arrays;

public class RemoveDuplicatesFromSortedArray {
    public int removeDuplicates(int[] arr) {
        int refIndex = 1; // index before which all are unique
        for (int i = 1; i < arr.length; i++) {
            if (arr[refIndex - 1] != arr[i]) {
                arr[refIndex] = arr[i];
                refIndex++;
            }
        }
        return refIndex;
    }

    public static void main(String[] args) {
        int arr[] = {10, 20, 20, 30, 30, 30};
        RemoveDuplicatesFromSortedArray sol = new RemoveDuplicatesFromSortedArray();
        System.out.println(sol.removeDuplicates(arr));
        System.out.println(Arrays.toString(arr));
    }
}

package com.ds.algos.trees;


public class TreeTraversal {

    // Order of complexity: O(n) where n is number of nodes
    public static void inOrder(TreeNode root) {
        if (root != null) {
            inOrder(root.left);
            System.out.print(root.value + " ");
            inOrder(root.right);
        }
    }

    // Order of complexity: O(n) where n is number of nodes
    public static void preOrder(TreeNode root) {
        if (root != null) {
            System.out.print(root.value + " ");
            preOrder(root.left);
            preOrder(root.right);
        }
    }

    // Order of complexity: O(n) where n is number of nodes
    public static void postOrder(TreeNode root) {
        if (root != null) {
            postOrder(root.left);
            postOrder(root.right);
            System.out.print(root.value + " ");
        }
    }

    public static void main(String[] args) {
        inOrder(DummyTrees.dummyTree1());
        System.out.println();
        preOrder(DummyTrees.dummyTree1());
        System.out.println();
        postOrder(DummyTrees.dummyTree1());
    }
}

package com.ds.algos.trees;


public class HeightOfBT {

    // order of complexity = O(n) where n is total nodes in BT
    public int getHeight(TreeNode root) {
        if (root == null) return 0;
        return 1 + Math.max(getHeight(root.left), getHeight(root.right));
    }
}

package com.ds.algos.trees;

public class DummyTrees {

    public static TreeNode dummyTree1() {
        TreeNode rootTen = new TreeNode(10);
        TreeNode twenty = new TreeNode(20);
        TreeNode thirty = new TreeNode(30);
        TreeNode forty = new TreeNode(40);
        TreeNode fifty = new TreeNode(50);

        rootTen.left = twenty;
        rootTen.right = thirty;
        twenty.left = forty;
        twenty.right = fifty;

        return rootTen;
    }
}

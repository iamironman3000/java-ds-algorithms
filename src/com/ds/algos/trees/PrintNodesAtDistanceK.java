package com.ds.algos.trees;

// Print nodes at distance k from root
// Order of complexity: O(h), h is height of tree
public class PrintNodesAtDistanceK {
    public static void printNodes(TreeNode root, int k) {
        if (root == null) return;
        if (k == 0) {
            System.out.println(root.value + " ");
        }

        printNodes(root.left, k - 1);
        printNodes(root.right, k - 1);
    }

    public static void main(String[] args) {
        printNodes(DummyTrees.dummyTree1(), 1);
    }
}
